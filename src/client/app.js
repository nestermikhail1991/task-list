import React from 'react';
import Task from './components/task';
import TaskInput from './components/taskInput';
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from 'react'
import {
    addTaskRemote,
    delTaskRemote,
    loadTasks,
    changeTaskRemote
} from "./redux/task-list/action";
import {loadUsers} from "./redux/users/action";
import {createUseStyles} from 'react-jss'


const useStyles = createUseStyles({
    app: {
        display: 'flex',
        'align-items': 'center',
        'flex-flow': 'column'
    }
});


export default function App() {

    const tasks = useSelector(state => state.tasks);
    const users = useSelector(state => state.users);


    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadUsers())
        dispatch(loadTasks())
    }, []);

    const addTaskAction = (task) => dispatch(addTaskRemote(task));

    const incPriorityAction = (task) => {
        const newTask = {...task};
        if (task.priority < 10){
            newTask.priority = ++task.priority;
        } else {
            newTask.priority = task.priority;
        }
            dispatch(changeTaskRemote(newTask));
    };

    const decPriorityAction = (task) => {
        const newTask = {...task};
        if (task.priority > 0){
            newTask.priority = --task.priority;
        } else {
            newTask.priority = task.priority;
        }
            dispatch(changeTaskRemote(newTask));
    };

    const deleteTaskAction = (task) => dispatch(delTaskRemote(task));

    const classes = useStyles();

    return (
        <div className={classes.app}>
            <div className={classes.newTask}>
                <TaskInput
                    addTask={task => addTaskAction(task)}
                    users={users}
                />
            </div>
            <div className={classes.tasks}>
                {tasks.slice(0).sort(((a, b) => b.priority - a.priority)).map(task => (
                    <Task
                        deleteTask={() => deleteTaskAction(task)}
                        incPriority={() => incPriorityAction(task)}
                        decPriority={() => decPriorityAction(task)}
                        task={task}
                        key={task.id}
                    />
                ))}
            </div>
        </div>
    )
}