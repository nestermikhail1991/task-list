import {ADD_TASK, DEL_TASK, UPDATE_TASK, SET_TASKS} from "./action-types";

export const addTask = (task) => ({
   type: ADD_TASK,
   payload: task
});

export const delTask = (task) => ({
    type: DEL_TASK,
    payload: task
});

export const updateTask = (task) => ({
    type: UPDATE_TASK,
    payload: task
});

export const setTasks = (tasks) => ({
    type: SET_TASKS,
    payload: tasks
});

export const loadTasks = () => async (dispatch, getState) => {

    try {
        const res = await fetch('/list_tasks');
        // console.log(res.status);
        const result = await res.json();
        dispatch(setTasks(result.tasks));
        // console.log(result);
    } catch (e) {
        console.error(e);
    }
};

export const addTaskRemote = (task) => async (dispatch) => {
    console.log(task);

    try {
        const res = await fetch('/add_task', {
            method: 'POST',
            body: JSON.stringify(task),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        // console.log(res.status);
        const newTask = await res.json();
        dispatch(addTask(newTask));
        // console.log(result);
    } catch (e) {
        console.error(e);
    }
};

export const delTaskRemote = (task) => async (dispatch) => {
    console.log(task);

    try {
        const res = await fetch(`/delete_task/${task.id}`, {
            method: 'DELETE',
            body: JSON.stringify(task)
        });
        // console.log(res.status);
        const result = await res.json();
        dispatch(delTask(task));
        // console.log(result);
    } catch (e) {
        console.error(e);
    }
};

export const changeTaskRemote = (task) => async (dispatch) => {
    console.log(task);

    try {
        const res = await fetch('/update_task', {
            method: 'POST',
            body: JSON.stringify(task),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        // console.log(res.status);
        const result = res.json();
        dispatch(updateTask(result));
        // console.log(result);
    } catch (e) {
        console.error(e);
    }
};