import {DEL_TASK, ADD_TASK, UPDATE_TASK, DEC_PRIORITY, SET_TASKS} from "./action-types";

export function tasks(state = [], action) {
    if (action.type === ADD_TASK) {
        return [...state, action.payload]
    }

    if (action.type === DEL_TASK) {
        const index = state.findIndex(t => t.id === action.payload.id);
        const newTasks = state.slice(0);
        newTasks.splice(index, 1);
        return newTasks
    }

    if (action.type === UPDATE_TASK) {
        const index = state.findIndex(t => t.id === action.payload.id);
        const newTasks = state.slice(0);
        // let newPrior = parseInt(newTasks[index].priority, 10);
        // if (newPrior < 10) {
        //     newPrior += 1;
        // }
        newTasks[index] = action.payload;
        return newTasks
    }

    // if (action.type === DEC_PRIORITY) {
    //     const index = state.indexOf(action.payload);
    //     const newTasks = state.slice(0);
    //     let newPrior = parseInt(newTasks[index].priority, 10);
    //     if (newPrior > 0) {
    //         newPrior -= 1;
    //     }
    //     newTasks[index].priority = newPrior;
    //     return newTasks
    // }
    if (action.type === SET_TASKS) {
        return action.payload
    }
    return state;
}