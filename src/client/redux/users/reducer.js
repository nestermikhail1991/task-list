import {SET_USERS} from "./action-types";

export function users(state = [], action) {
    if (action.type === SET_USERS) {
        return action.payload
    }
    return state;
}