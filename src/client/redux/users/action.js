import {SET_USERS} from "./action-types";

export const setUsers = (users) => ({
    type: SET_USERS,
    payload: users
});

export const loadUsers = () => async (dispatch, getState) => {

    try {
        const res = await fetch('/list_users');
        // console.log(res.status);
        const result = await res.json();
        dispatch(setUsers(result.users));
        // console.log(result);
    } catch (e) {
        console.error(e);
    }
};