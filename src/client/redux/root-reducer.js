import {combineReducers} from 'redux';
import {tasks} from "./task-list/reducer";
import {users} from "./users/reducer";

export const rootReducer = combineReducers({
    tasks,
    users
});