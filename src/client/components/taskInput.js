import React from 'react';

const TaskInputStyle = {
        height: '150px',
        width: '450px',
        border: 'lightgrey 1px solid',
        margin: '20px',
        padding: '5px',
        boxShadow: '0 14px 28px rgba(0,0,0,0.25), 0 5px 5px rgba(0,0,0,0.22)',
        display: 'flex',
        flexFlow: 'column',
    };

const InputDescriptionStyle = {
        height: '70px',
    };

const InputStyle = {
        marginTop: '4px',
        marginBottom: '4px',
    };



class TaskInput extends React.Component {
    constructor(props) {
        super(props);
        this.inputDescription = React.createRef();
        this.inputExecutor = React.createRef();
        this.inputPriority = React.createRef();

        this.addTask = this.addTask.bind(this)
    }

    addTask() {
        // console.log(this.inputDescription);
        const description = this.inputDescription.current.value;
        const executor = this.inputExecutor.current.value;
        const priority = this.inputPriority.current.value;

        if (description.length !== 0) {
            this.props.addTask({
                description: description,
                executor: executor,
                priority: priority
            });
        }
    };

    render() {

        const users = this.props.users;
        // console.log(users);

        return(
            <div style={TaskInputStyle}>
                <textarea style={InputStyle && InputDescriptionStyle}
                          ref={this.inputDescription}
                          placeholder="Описание"
                />
                <select style={InputStyle}
                        ref={this.inputExecutor}
                >

                    {users.map(user => <option key={user}>{user}</option>)}

                </select>
                <input style={InputStyle}
                       type="number"
                       placeholder="Приоритет"
                       ref={this.inputPriority}
                       min={0}
                       max={10}
                       defaultValue={0}
                />
                <button onClick={() =>this.addTask()}>Добавить задачу</button>
            </div>
        )
    }
}

export default TaskInput