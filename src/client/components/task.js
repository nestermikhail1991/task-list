import React from 'react';
import {createUseStyles} from 'react-jss'

const useStyles = createUseStyles({
    task: {
        height: 110,
        width: 450,
        border: 'lightgrey 1px solid',
        margin: 20,
        padding: 5,
        'box-shadow': '0 14px 28px rgba(0,0,0,0.25), 0 5px 5px rgba(0,0,0,0.22)',
        position: 'relative',
    },
    button: {
        margin: 3,
        'background-color': 'rgba(0,0,0,0.1)',
        height: 28,
        'border-radius': 3
    },
    buttons: {
        margin: 8,
        position: 'absolute',
        bottom: 0,
        right: 0
    },
    tasks: {
        margin: {
            top: 16,
            right: 0,
            bottom: 16,
            left: 8
        }
    }
});


const Task = (props) => {
    const classes = useStyles();
    return(
        <div className={classes.task}>
            <div className={classes.info}>
                <p className={classes.tasks}>{props.task.description}</p>
                <p className={classes.tasks}>{props.task.executor}</p>
                <p className={classes.tasks}>{props.task.priority}</p>
            </div>
            <div className={classes.buttons}>
                <button className={classes.button} onClick={props.deleteTask}>Удалить</button>
                <button className={classes.button} onClick={props.decPriority}>Понизить приоритет</button>
                <button className={classes.button} onClick={props.incPriority}>Повысить приоритет</button>
            </div>
        </div>
    )
};

export default Task