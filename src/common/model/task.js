export class Task {

    constructor(id, description, executor, priority) {
        this.id = id;
        this.description = description;
        this.executor = executor;
        this.priority = priority;
    }
}
