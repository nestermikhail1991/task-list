import {Task} from "../common/model/task";

const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = 5001;

app.use(bodyParser.json({ type: 'application/json'}));
app.use(express.static('build/client'));
app.use('/public', express.static('public'));

const USERS = [
    'Петр',
    'Валентин',
    'Алла'
];

const TASKS = [
    new Task(0, 'Описание задачи - 0', 'Петр', 0),
    new Task(1, 'Описание задачи - 1', 'Валентин', 1),
    new Task(2, 'Описание задачи - 2', 'Алла', 2),
];

let taskId = 3;

// const PRIORITY = Task.map(task => {return task.priority});

app.get('/list_users', (req, res) => res.json({users: USERS}));
app.get('/list_tasks', (req, res) => res.json({tasks: TASKS}));

app.post('/add_task', (req, res) => {
    // console.log(req.body);
    const newTask = {...req.body, id: taskId++};
    TASKS.push(newTask);
    res.json(newTask)
});

app.post('/update_task', (req, res) => {
    const index = TASKS.findIndex(t => t.id === req.body.id);
    TASKS[index] = req.body;
    res.json({success: true})
});

app.delete('/delete_task/:id', (req, res) => {
    const index = TASKS.findIndex(t => t.id === req.body.id);
    // console.log(TASKS[index]);
    // console.log(req.params.id);
    TASKS.splice(index, 1);
    res.json({success: true})
});

app.listen(PORT, () => console.log(`App listening at http://localhost:${PORT}`));
