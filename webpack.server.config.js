const path = require('path');


module.exports = {
    target: 'node',
    entry: [
        './src/server/index.js',
    ],
    output: {
        filename: 'index.js',
        path: path.resolve(__dirname, 'build/server')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            }
        ]
    },
    plugins: [

    ]
};